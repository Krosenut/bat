package dev.buildtool.bat;

import java.util.Objects;

public class Dependency {
    String group;
    String artifact;
    SemanticVersion version;
    Scope scope = Scope.COMPILE;
    String classifier = "";

    public Dependency(String group, String artifact, SemanticVersion version) {
        this.group = group;
        this.artifact = artifact;
        this.version = version;
    }

    public Dependency(String group, String artifact, SemanticVersion version, String classifier) {
        this.group = group;
        this.artifact = artifact;
        this.version = version;
        this.classifier = classifier;
    }

    public Dependency(String group, String artifact, SemanticVersion version, Scope scope, String classifier) {
        this.group = group;
        this.artifact = artifact;
        this.version = version;
        this.scope = scope;
        this.classifier = classifier;
    }

    @Override
    public String toString()
    {
        return "{" +
                "group='" + group + '\'' +
                ", artifact='" + artifact + '\'' +
                ", version=" + version +
                ", scope=" + scope +
                ", classifier=" + classifier +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dependency that = (Dependency) o;
        return group.equals(that.group) &&
                artifact.equals(that.artifact) &&
                version.equals(that.version) &&
                scope == that.scope && classifier.equals(that.classifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, artifact, version, scope, classifier);
    }

    public String toRelativePath() {
        String directory = group.replace('.', '/') + "/" + artifact + "/" + version + "/";
        return directory + artifact + "-" + version + (classifier.equals("") ? "" : "-" + classifier);
    }
}

enum Scope {
    COMPILE("compile", "Applicable to all classpaths"),
    PROVIDED("provided", "Applicable to compilation and test classpaths, but not included by the project"),
    RUNTIME("runtime", "Applicable to runtime and test classpaths"),
    TEST("test", "Applicable to test classpath");

    String value, description;

    Scope(String value, String description) {
        this.value = value;
        this.description = description;
    }
}

