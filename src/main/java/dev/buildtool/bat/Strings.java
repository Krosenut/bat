package dev.buildtool.bat;

/**
 * Created on 5/8/20.
 */
public class Strings {
    public enum ProjectProperty {
        PROJECT_GROUP("group_id"),
        PROJECT_VERSION("version"),
        PROJECT_ARTIFACT("artifact_id"),
        SOURCE_DIRECTORY("source_dir"),
        OUTPUT_DIRECTORY("output_dir"),
        MAIN_CLASS("main_class"),
        TARGET_BYTECODE_VERSION("target_bytecode"),
        PROJECT_LAYOUT("layout"),
        ARGUMENTS("args"),
        JAR_DIR("jar_dir"),
        IMAGE_DIR("image_dir"),
        ADDITIONAL_REPOSITORIES("repositories"),
        PROJECTS("projects"),
        RESOURCE_DIR("resources"),
        DEPENDENCIES("dependencies");
        public String property;

        ProjectProperty(String property) {
            this.property = property;
        }
    }

    public enum MavenCoordinates {
        CLASSIFIER("classifier"),
        SCOPE("scope"),
        ARTIFACT("artifactId"),
        GROUP("groupId"),
        VERSION(ProjectProperty.PROJECT_VERSION.property);
        public String value;

        MavenCoordinates(String value) {
            this.value = value;
        }
    }

    static final String MAVEN_CENTRAL = "https://repo1.maven.org/maven2/";
    static final String BUILD_FILE = "project.json5", BAT_DIR = ".bat";
    static final String PROJECT_LOCATION = "project_location";
    public static final String COMPACT = "compact";
    public static final String BROAD = "broad";
    static final String LAST_PROJ_DIR = "last_project_dir";
    protected static final String LAST_SELECTED_DIRECTORY = "Last selected directory";
}
