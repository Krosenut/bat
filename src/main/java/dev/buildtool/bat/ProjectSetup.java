package dev.buildtool.bat;

import dev.buildtool.json.SyntaxError;
import dev.buildtool.tools.collections.UniqueItemList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.VBox;
import javafx.util.Pair;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

//should be rewritten - SetupProject
public class ProjectSetup implements EventHandler<ActionEvent>
{
    protected BAT bat;

    public ProjectSetup(BAT bat) {
        this.bat = bat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void handle(ActionEvent event) {
        bat.dependencyLocations.clear();
        bat.linkedProjects.clear();
        try {
            LinkedHashMap<String, Object> linkedHashMap = bat.parseProjectProperties();
            final Path projectDir = (Path) linkedHashMap.get(Strings.PROJECT_LOCATION);
            List<String> projects = (List<String>) linkedHashMap.get(Strings.ProjectProperty.PROJECTS.property);

            if (projects != null && !projects.isEmpty()) {
                projects.forEach(s -> {
                    Path projectFile = Path.of(projectDir.toString(), s, Strings.BUILD_FILE);
                    if (Files.exists(projectFile)) {
                        bat.linkedProjects.add(projectFile);
                        new Alert2(Alert.AlertType.INFORMATION, "Found and added setup file " + projectFile.normalize()).show();
                    }
                });
            }
            List<Map<String, String>> dependencies = (List<Map<String, String>>) linkedHashMap.getOrDefault(Strings.ProjectProperty.DEPENDENCIES.property, new UniqueItemList<>());

            bat.linkedProjects.forEach(path -> {
                try {
                    Map<String, Object> projectProps = bat.parseProjectProperties(path);
                    if (projectProps.containsKey(Strings.ProjectProperty.DEPENDENCIES.property)) {
                        List<Map<String, String>> transitDependencies = (List<Map<String, String>>) projectProps.get(Strings.ProjectProperty.DEPENDENCIES.property);
                        dependencies.addAll(transitDependencies);
                    }
                } catch (SyntaxError syntaxError) {
                    syntaxError.printStackTrace();
                }
            });

            if (linkedHashMap.containsKey(Strings.ProjectProperty.DEPENDENCIES.property) || !dependencies.isEmpty()) {
                Path localMaven = BAT.LOCAL_MAVEN;
                if (Files.notExists(localMaven)) {
                    Files.createDirectories(localMaven);
                }
                UniqueItemList<String> extraRepositories = new UniqueItemList<>(Strings.MAVEN_CENTRAL);
                if (linkedHashMap.containsKey(Strings.ProjectProperty.ADDITIONAL_REPOSITORIES.property)) {
                    List<String> repoUrls = (List<String>) linkedHashMap.get(Strings.ProjectProperty.ADDITIONAL_REPOSITORIES.property);
                    extraRepositories.addAll(repoUrls);
                }

                Dialog<Boolean> dialog = new Dialog<>();
                ProgressIndicator indicator = new ProgressIndicator();
                dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.FINISH);
                VBox content = new VBox(6, indicator);
                dialog.getDialogPane().setContent(content);
                dialog.setTitle("Status");
                dialog.setHeaderText("Resolving dependencies");
                dialog.setResultConverter(param -> param == ButtonType.FINISH);
                Task<Object> resolution = new Task<>() {
                    @Override
                    protected Object call() {

                        for (Map<String, String> dependency : dependencies) {
                            if (isCancelled()) {
                                break;
                            }
                            final String group = dependency.get(Strings.ProjectProperty.PROJECT_GROUP.property);
                            final String artifact = dependency.get(Strings.ProjectProperty.PROJECT_ARTIFACT.property);
                            final String version = dependency.get(Strings.ProjectProperty.PROJECT_VERSION.property);
                            final String classifier = dependency.getOrDefault(Strings.MavenCoordinates.CLASSIFIER.value, "");
                            final String scope = dependency.getOrDefault(Strings.MavenCoordinates.SCOPE.value, "COMPILE");
                            for (String extraRepository : extraRepositories) {
                                Dependency resolved = resolve(group, artifact, version, classifier, false, Scope.valueOf(scope), null, null, null, extraRepository);
                                if (resolved != null)
                                    break;
                            }
                        }
                        return null;
                    }
                };
                resolution.setOnSucceeded(event1 -> {
                    dialog.close();
                    new Alert2(Alert.AlertType.INFORMATION, "Resolved " + bat.dependencyLocations.size() + " dependencies", ButtonType.FINISH).show();
                });
                resolution.setOnFailed(event1 -> {
                    content.getChildren().remove(indicator);
                    dialog.setHeaderText("Resolution failed");
                });
                Thread thread = new Thread(resolution);
                thread.start();

                Optional<Boolean> cancelled = dialog.showAndWait();
                if (cancelled.isPresent() && cancelled.get()) {
                    thread.interrupt();
                    resolution.cancel();
                    dialog.close();
                    new Alert2(Alert.AlertType.WARNING, "Resolution interrupted", ButtonType.FINISH).show();
                }
            }

            String sourceDir = (String) linkedHashMap.getOrDefault(Strings.ProjectProperty.SOURCE_DIRECTORY.property, "src");
            Path srcPath = Path.of(projectDir.toString(), sourceDir);
            if (Files.notExists(srcPath)) {
                Files.createDirectory(srcPath);
            }

            final String layout = (String) linkedHashMap.getOrDefault(Strings.ProjectProperty.PROJECT_LAYOUT.property, Strings.COMPACT);

            String group = (String) linkedHashMap.getOrDefault(Strings.ProjectProperty.PROJECT_GROUP.property, "");
            String artifact = (String) linkedHashMap.getOrDefault(Strings.ProjectProperty.PROJECT_ARTIFACT.property, "");
            if (!group.isEmpty() && !artifact.isEmpty()) {
                Path mainPackage;
                if (layout.equals(Strings.COMPACT)) {
                    mainPackage = Path.of(srcPath.toString(), group.replace('.', '/'), artifact.replace('-', '/'));
                } else {
                    mainPackage = Path.of(srcPath.toString(), group, group.replace('.', '/'), artifact.replace('-', '/'));
                }
                if (Files.notExists(mainPackage)) {
                    Files.createDirectories(mainPackage);
                }
            }

            String classDir = (String) linkedHashMap.getOrDefault(Strings.ProjectProperty.OUTPUT_DIRECTORY.property, "classes");
            Path classes = Path.of(projectDir.toString(), classDir);
            if (Files.notExists(classes)) {
                Files.createDirectory(classes);
            }

            bat.projectSetUp = true;
        } catch (Exception e) {
            e.printStackTrace();
            bat.projectSetUp = false;
        }
    }

    private Profiles getProfiles(Element mainDocumentElement) {
        Profiles ps = new Profiles();
        NodeList profiles = mainDocumentElement.getElementsByTagName("profile");
        for (int next = 0; next < profiles.getLength(); next++) {
            Profile profile = new Profile();
            Element nextNode = (Element) profiles.item(next);
            Element properties = (Element) nextNode.getElementsByTagName("properties").item(0);
            if (properties != null) {
                NodeList profPropList = properties.getElementsByTagName("*");
                for (int i1 = 0; i1 < profPropList.getLength(); i1++) {
                    Element nextProfProperty = (Element) profPropList.item(i1);
                    profile.addProperty(nextProfProperty.getTagName(), nextProfProperty.getTextContent());
                }

            }

            Node id = nextNode.getElementsByTagName("id").item(0);
            if (id != null) {
                profile.setIdentifier(id.getTextContent());
            }

            Node activation = nextNode.getElementsByTagName("activation").item(0);
            if (activation != null) {
                Profile.ActivationTrigger activationTrigger = new Profile.ActivationTrigger();
                for (int i = 0; i < activation.getChildNodes().getLength(); i++) {
                    Node chNode = activation.getChildNodes().item(i);
                    if (chNode instanceof Element) {
                        Element chElement = (Element) chNode;
                        String tagName = chElement.getTagName();
                        if (tagName.equals("os")) {
                            String osName = getTagValue(chElement, Profile.OS_NAME);
                            String osArch = getTagValue(chElement, Profile.OS_ARCHITECTURE);
                            String osFamily = getTagValue(chElement, Profile.OS_FAMILY);
                            String osVersion = getTagValue(chElement, Profile.OS_VERSION);
                            activationTrigger.setOS(osName, osArch, osVersion, osFamily);
                            profile.setActivationTrigger(activationTrigger);
                        }
                    }
                }
            }
            ps.addProfile(profile);
        }
        return ps;
    }

    /**
     * @return tag value or empty string
     */
    public static String getTagValue(Node node, String tagName) {
        if (node instanceof Element) {
            Element el = (Element) node;
            Node subNode = el.getElementsByTagName(tagName).item(0);
            if (subNode != null) {
                return subNode.getTextContent();
            }
        }
        return "";
    }

    /**
     * Downloads file from the Maven repository if it doesn't exist locally
     *
     * @param remotePath     relative file path
     * @param repositoryRoot
     * @return local file path and whether the file already existed
     */
    private Pair<String, Boolean> download(String remotePath, String repositoryRoot) {
        String localPath = BAT.LOCAL_MAVEN + "/" + remotePath;
        try {
            final Path local = Path.of(localPath);
            if (Files.notExists(local)) {
                Files.createDirectories(local.getParent());
                URL url = new URL(repositoryRoot + remotePath);
                System.out.println("Downloading " + url);
                InputStream inputStream = url.openStream();
                ReadableByteChannel readableByteChannel = Channels.newChannel(inputStream);
                FileOutputStream outputStream = new FileOutputStream(localPath);
                FileChannel channel = outputStream.getChannel();
                channel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
                channel.close();
                System.out.println("Finished");
                return new Pair<>(localPath, false);
            } else {
                System.out.println(localPath + " exists locally");
                return new Pair<>(localPath, true);
            }
        } catch (FileNotFoundException f) {
            System.out.println(remotePath + " doesn't exist");
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String parseNodeContent(Node node) {
        String value = node.getTextContent();
        if (value.startsWith("$")) {
            return value.substring(2, value.length() - 1);
        }
        return value;
    }

    /**
     * Recursively resolves dependencies and downloads their artifacts
     *
     * @param repositoryRoot
     * @param isParent       whether this is a parent (transitive) dependency
     * @return resolved dependency or null to prevent further resolution
     */
    private Dependency resolve(String group, String artifact, String version, String classifier, boolean isParent, Scope scope_, String prevGroup, String prevArtifact, String prevVersion, String repositoryRoot) {
        try {
            if (group.equals("${project.groupId}"))
                group = prevGroup;
            if (version.equals("${project.version}"))
                version = prevVersion;
            SemanticVersion artifactVersion = new SemanticVersion(version);
            if (artifactVersion.toString().equals("0.0.0")) {
                System.out.println("Non-semantic version: " + version + " of artifact " + artifact);
                return null;
            }
            Dependency declaredDependency = new Dependency(group, artifact, artifactVersion, scope_, classifier);
            String classifierSuffix = "";
            if (!classifier.isEmpty()) {
                classifierSuffix = "-" + classifier;
            }

            if (group.startsWith("$")) {
                Connection.Response metadata = Jsoup.connect(repositoryRoot + group.replace('.', '/') + "/" + artifact + "/maven-metadata.xml").method(Connection.Method.GET).execute();
                final Elements releaseTag = metadata.parse().getElementsByTag("groupId");
                if (releaseTag != null) {
                    org.jsoup.nodes.Element release = releaseTag.get(0);
                    group = release.text();
                }
            }
            if (version.startsWith("$")) {
                Connection.Response metadata = Jsoup.connect(repositoryRoot + group.replace('.', '/') + "/" + artifact + "/maven-metadata.xml").method(Connection.Method.GET).execute();
                final Elements releaseTag = metadata.parse().getElementsByTag("release");
                if (releaseTag != null && releaseTag.size() > 0) {
                    org.jsoup.nodes.Element release = releaseTag.get(0);
                    version = release.text();
                }
            }
            assert !group.startsWith("$") && !version.startsWith("$");
            String directory = group.replace('.', '/') + "/" + artifact + "/" + version + "/";
            String jarPath = directory + artifact + "-" + version + classifierSuffix + ".jar";
            String pomPath = directory + artifact + "-" + version + ".pom";
            String sourceJar = directory + artifact + "-" + version + "-sources.jar";
            String localDestination = BAT.LOCAL_MAVEN + "/" + directory;

            Path localDestinationPath = Path.of(localDestination);
            if (Files.notExists(localDestinationPath)) {
                Files.createDirectories(localDestinationPath);
            }

            //download binary Jar
            Pair<String, Boolean> localJar = download(jarPath, repositoryRoot);
            if (localJar != null) {
                if (bat.dependencyLocations.containsKey(declaredDependency)) {
                    return null;
                }
                bat.dependencyLocations.put(declaredDependency, Path.of(localJar.getKey()).toAbsolutePath().toString());
            }

            //download source jar
            if (!isParent && (scope_ == Scope.COMPILE || scope_ == Scope.PROVIDED)) {
                download(sourceJar, repositoryRoot);
            }
            //POM download
            Pair<String, Boolean> localPom = download(pomPath, repositoryRoot);
            if (localPom != null) {
                //POM parsing
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newDefaultInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document pom = documentBuilder.parse(localPom.getKey());
                Element mainElement = pom.getDocumentElement();
                NodeList nodeListProps = mainElement.getElementsByTagName("properties");
                HashMap<String, String> propertyMap = new HashMap<>();
                if (nodeListProps.getLength() > 0) {
//                    System.out.println("Found " + nodeListProps.getLength() + " properties");
                    Node nodeProperty = nodeListProps.item(0);
                    for (int i = 0; i < nodeProperty.getChildNodes().getLength(); i++) {
                        Node propNode = nodeProperty.getChildNodes().item(i);
                        String nodeName = propNode.getNodeName();
                        if (!nodeName.startsWith("#")) {
                            String textContent = propNode.getTextContent();
                            propertyMap.put(nodeName, textContent);
                        }
                    }
                }
                if (!propertyMap.isEmpty()) {
                    System.out.println("Gathered properties: " + propertyMap.size());
                }
                NodeList modules = mainElement.getElementsByTagName("modules");
                if (modules.getLength() > 0) {
                    Node moduleList = modules.item(0);
                    System.out.println("Found " + moduleList.getChildNodes().getLength() + " modules");
                    for (int i = 0; i < moduleList.getChildNodes().getLength(); i++) {
                        Node module = moduleList.getChildNodes().item(i);
                        if (module != null && module.getNodeName().equals("#text")) {
                            String moduleString = module.getTextContent();
                            if (!moduleString.isBlank()) {
                                moduleString = moduleString.strip().replaceAll("[\n\t]", "");
                                if (moduleString.lastIndexOf("$") > 0)
                                    continue;
                                if (moduleString.startsWith("$")) {
                                    moduleString = getProperty(declaredDependency, documentBuilderFactory, moduleString);
                                }
                                System.out.println("Resolving module " + moduleString);
                                Dependency dependency = resolve(group, moduleString, version, classifier, isParent, scope_, prevGroup, prevArtifact, prevVersion, repositoryRoot);
                                if (dependency != null && dependency.scope != Scope.TEST && !bat.dependencyLocations.containsKey(dependency)) {
                                    bat.dependencyLocations.put(dependency, Path.of(dependency.toRelativePath() + ".jar").toAbsolutePath().toString());
                                }
                            }
                        }

                    }
                }

                Element parent = (Element) mainElement.getElementsByTagName("parent").item(0);
                //parent POM parsing
                Profiles profiles = null;
                if (parent != null) {
                    System.out.println("Found parent POM for " + declaredDependency);
                    Node parentGroup = parent.getElementsByTagName(Strings.MavenCoordinates.GROUP.value).item(0);
                    Node parentArtifact = parent.getElementsByTagName(Strings.MavenCoordinates.ARTIFACT.value).item(0);
                    Node parentVersion = parent.getElementsByTagName(Strings.ProjectProperty.PROJECT_VERSION.property).item(0);

                    Node depPackaging = mainElement.getElementsByTagName("packaging").item(0);
                    if (depPackaging != null /*&& depPackaging.getTextContent().equals("pom")*/) {
                        String packagingTextContent = depPackaging.getTextContent();
                        if (packagingTextContent.equals("pom")) {
//                            System.out.println("Skipping pom");
//                            return null;
                        }
                    }

                    String groupTextContent = parentGroup.getTextContent();
                    if (groupTextContent.startsWith("$"))
                        groupTextContent = propertyMap.get(groupTextContent);
                    String artifactTextContent = parentArtifact.getTextContent();
                    String versionTextContent = parentVersion.getTextContent();
                    Dependency parentDependency = resolve(groupTextContent, artifactTextContent, versionTextContent, "", true, Scope.COMPILE, group, artifact, version, repositoryRoot);

                    if (parentDependency != null) {

                        String parentPom = BAT.LOCAL_MAVEN + "/" + (parentDependency.toRelativePath() + ".pom");
                        Document parentDocument = documentBuilderFactory.newDocumentBuilder().parse(parentPom);
                        Element mainDocumentElement = parentDocument.getDocumentElement();
                        profiles = getProfiles(mainDocumentElement);
                        bat.dependencyLocations.put(parentDependency, Path.of(parentDependency.toRelativePath()).toAbsolutePath() + "/.jar");
                    }
                }

                //super dependency parsing
                Node node = null;
                NodeList nodeList = mainElement.getElementsByTagName(Strings.ProjectProperty.DEPENDENCIES.property);

                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node next = nodeList.item(i);
                    Node parentNode = next.getParentNode();
                    if (parentNode.getNodeName().equals("project")) {
                        node = next;
                        break;
                    }
                }

                if (node != null && declaredDependency.scope != Scope.TEST) {
                    System.out.println("Found dependencies of " + declaredDependency);
                    NodeList dependencyNodes = node.getChildNodes();
                    for (int i = 0; i < dependencyNodes.getLength(); i++) {
                        Node depNode = dependencyNodes.item(i);
                        if (depNode instanceof Element) {
                            Element depElement = (Element) depNode;
                            Node depGroup = depElement.getElementsByTagName(Strings.MavenCoordinates.GROUP.value).item(0);
                            Node depArtifact = depElement.getElementsByTagName(Strings.MavenCoordinates.ARTIFACT.value).item(0);
                            Node depVersion = depElement.getElementsByTagName(Strings.ProjectProperty.PROJECT_VERSION.property).item(0);
                            Node depClassifier = depElement.getElementsByTagName(Strings.MavenCoordinates.CLASSIFIER.value).item(0);
                            Node depScope = depElement.getElementsByTagName("scope").item(0);
                            Node depIsOptional = depElement.getElementsByTagName("optional").item(0);
                            if (depIsOptional != null && depIsOptional.getTextContent().equals("true")) {
                                System.out.println("Skipping optional dependency " + depGroup.getTextContent() + " " + depArtifact.getTextContent());
                                continue;
                            }
                            Node depType = depElement.getElementsByTagName("type").item(0);
                            if (depType != null && depType.getTextContent().equals("zip")) {
                                System.out.println("Skipping dependency of type ZIP");
                                continue;
                            }

                            String depGroupValue = depGroup.getTextContent();
                            String depArtifactValue = depArtifact.getTextContent();
                            String depClassifierString = "";
                            Scope scope = Scope.COMPILE;
                            if (depScope != null) {
                                String depScopeValue = depScope.getTextContent();
                                switch (depScopeValue) {
                                    case "test":
                                        scope = Scope.TEST;
                                        break;
                                    case "provided":
                                        scope = Scope.PROVIDED;
                                        break;
                                    case "runtime":
                                        scope = Scope.RUNTIME;
                                        break;
                                }
                            }
                            if (depClassifier != null) {
                                depClassifierString = depClassifier.getTextContent();
                                if (depClassifierString.startsWith("$")) {
                                    String variable = parseNodeContent(depClassifier);
                                    System.out.println("Detected variable classifier: " + variable);
                                    if (profiles != null) {
                                        for (Profile profile : profiles.getProfiles()) {
                                            String propertyValue = profile.getProperty(variable);
                                            if (propertyValue != null) {
                                                Profile.ActivationTrigger activationTrigger = profile.getActivationTrigger();
                                                if (activationTrigger != null) {
                                                    if (activationTrigger.match()) {
                                                        System.out.println("Profile with id '" + profile.getIdentifier() + "' matches the '" + variable + "' variable");
                                                        depClassifierString = profile.getProperty(variable);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            String depVersionString = null;
                            //if version is unspecified, get the last "release" version from metadata file
                            if (depVersion == null) {
                                if (scope != Scope.TEST) {
                                    Connection.Response metadata = Jsoup.connect(repositoryRoot + depGroupValue.replace('.', '/') + "/" + depArtifactValue + "/maven-metadata.xml").method(Connection.Method.GET).execute();
                                    final Elements releaseTag = metadata.parse().getElementsByTag("release");
                                    if (releaseTag != null && releaseTag.size() > 0) {
                                        org.jsoup.nodes.Element release = releaseTag.get(0);
                                        System.out.println("(Super dependency)");
                                        depVersionString = release.text();
                                    }
                                }
                            } else {
                                depVersionString = depVersion.getTextContent();
                                if (depVersionString.startsWith("$")) {
                                    depVersionString = getProperty(declaredDependency, documentBuilderFactory, depVersionString);
                                }
                            }
                            if (depVersionString == null) {
                                System.out.println("Version of " + depGroup.getTextContent() + " " + depArtifactValue + " is null");
                                continue;
                            }
                            Dependency superDependency = resolve(depGroup.getTextContent(), depArtifactValue, depVersionString, depClassifierString == null ? "" : depClassifierString, false, scope, group, artifact, version, repositoryRoot);
                            if (superDependency != null) {
                                //TODO cache
                                if (!bat.dependencyLocations.containsKey(superDependency)) {
                                    bat.dependencyLocations.put(superDependency, Path.of(superDependency.toRelativePath() + ".jar").toAbsolutePath().toString());
                                }
                            }
                        }
                    }
                }
            }
            return declaredDependency;
        } catch (SAXParseException saxParseException) {
            System.out.println(saxParseException.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("On: " + group + " " + artifact + " " + version);
            System.exit(1);
        }
        return null;
    }

    private String getProperty(Dependency declaredDependency, DocumentBuilderFactory documentBuilderFactory, String propertyNotation) throws SAXException, IOException, ParserConfigurationException {
        String depPom = BAT.LOCAL_MAVEN + "/" + declaredDependency.toRelativePath() + ".pom";
        if (Files.exists(Path.of(depPom))) {
            Node depProperties = documentBuilderFactory.newDocumentBuilder().parse(depPom).getElementsByTagName("properties").item(0);
            if (depProperties != null) {
                final NodeList childNodes = depProperties.getChildNodes();
                for (int i1 = 0; i1 < childNodes.getLength(); i1++) {
                    Node item = childNodes.item(i1);
                    if (item instanceof Element) {
                        Element element = (Element) item;
                        String nextPropertyName = element.getTagName();
                        String nextPropertyValue = element.getTextContent();
                        if (nextPropertyName.equals(propertyNotation.substring(2, propertyNotation.length() - 1))) {
                            propertyNotation = nextPropertyValue;
                            break;
                        }
                    }
                }
            }
        }
        return propertyNotation;
    }

    public static int findCharBackwards(String s, char c, int occurence) {
        int occ = 0;
        for (int i = s.length() - 1; i > 0; i--) {
            char next = s.charAt(i);
            if (next == c)
                occ++;
            if (occ == occurence)
                return i;
        }
        return -1;
    }
}
