package dev.buildtool.bat;

import javafx.scene.text.Text;

import java.io.OutputStream;

/**
 * Passes data to BAT's output pane
 */
class TextFlowOutput extends OutputStream
{
    private final BAT bat;

    public TextFlowOutput(BAT bat)
    {
        this.bat = bat;
    }

    String line = "";

    @Override
    public void write(int b)
    {
        char ch = (char) b;
        line += ch;
        if (ch == '\n')
        {
            bat.processOutput.getChildren().add(new Text(line));
            line = "";
        }
    }
}
