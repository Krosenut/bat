package dev.buildtool.bat;

import dev.buildtool.json.SyntaxError;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.text.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Java runner. Uses processes because runtime is not available from tool provider
 */
class RunProject implements EventHandler<ActionEvent> {
    private final BAT bat;

    public RunProject(BAT bat) {
        this.bat = bat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void handle(ActionEvent event) {
        if (bat.projectSetUp) {
            try {
                Map<String, Object> properties = bat.parseProjectProperties();
                if (properties != null) {
                    final String mainClass = (String) properties.get(Strings.ProjectProperty.MAIN_CLASS.property);
                    if (mainClass.isEmpty()) {
                        new Alert2(Alert.AlertType.WARNING, "Main class not specified", "Specify \"" + Strings.ProjectProperty.MAIN_CLASS.property + "\" value (relative path)", ButtonType.OK).show();
                    } else {
                        final String layout = (String) properties.getOrDefault(Strings.ProjectProperty.PROJECT_LAYOUT.property, Strings.COMPACT);
                        String javaHome = BAT.JAVA_HOME;
                        System.out.println("Java installation: " + javaHome);
                        final Path projectDir = (Path) properties.get(Strings.PROJECT_LOCATION);
                        String sourceDir = projectDir.toString() + "/" + properties.get(Strings.ProjectProperty.SOURCE_DIRECTORY.property);

                        Path mainClassPath = Path.of(sourceDir, mainClass);
                        System.out.println("Main class: " + mainClassPath);

                        String classes = (String) properties.getOrDefault(Strings.ProjectProperty.OUTPUT_DIRECTORY.property, "classes");
                        boolean inModule = Files.exists(Path.of(projectDir.toString(), classes, "module-info.class"), LinkOption.NOFOLLOW_LINKS);
                        final String projectVersion = (String) properties.get(Strings.ProjectProperty.PROJECT_VERSION.property);
                        //construct correct main class name
                        String moduledClassName = "";
                        final String group = (String) properties.get(Strings.ProjectProperty.PROJECT_GROUP.property);
                        if (inModule) {
                            if (layout.equals(Strings.COMPACT)) {
                                String rightPart = mainClass.substring(mainClass.indexOf('/') + 1).replace('/', '.').replace(".java", "");
                                moduledClassName = (mainClass.substring(0, mainClass.indexOf('/')) + "/" + rightPart);
                            } else {
                                moduledClassName = mainClass.substring(0, mainClass.indexOf('/')) + "/" + mainClass.substring(mainClass.indexOf("/") + 1).replace('/', '.').replace(".java", "").replace(group + ".", "");
                            }
                        }

                        StringBuilder dependencies = new StringBuilder();
                        HashSet<Dependency> set = new HashSet<>(bat.dependencyLocations.keySet());
                        bat.dependencyLocations.forEach((dependency, s) -> {
                            if (set.contains(dependency) && dependency.scope != Scope.TEST)
                                dependencies.append(s).append(BAT.PATH_SEPARATOR);
                        });
                        String arguments = (String) properties.get(Strings.ProjectProperty.ARGUMENTS.property);
                        String argString = "";
                        if (arguments != null) {
                            argString = String.join(" ", arguments);
                        }
                        Process process;
                        ProcessBuilder processBuilder;
                        //launch from directory
                        if (bat.toggleGroup.getSelectedToggle() == bat.directory) {
                            if (inModule)
                                processBuilder = new ProcessBuilder("java", "--module-path", dependencies.toString() + projectDir.resolve(classes), "--module", moduledClassName, argString);
                            else
                                processBuilder = new ProcessBuilder("java", "-cp", dependencies.toString() + projectDir.resolve(classes), mainClass.replace("/", ".").replace(".java", ""), argString);
                        } else {
                            //jar launch
                            String jarDir = (String) properties.getOrDefault(Strings.ProjectProperty.JAR_DIR.property, "jars");
                            Path jarDirPath = Path.of(jarDir);
                            if (Files.notExists(jarDirPath)) {
                                Files.createDirectory(jarDirPath);
                            }
                            if (inModule)
                                processBuilder = new ProcessBuilder("java", "--module-path", dependencies.toString() + projectDir.resolve(jarDir), "--module", moduledClassName, argString);
                            else {
                                processBuilder = new ProcessBuilder("java", "-jar", Arrays.stream(projectDir.resolve(jarDir).toFile().listFiles()).filter(file -> file.getName().contains(projectVersion)).collect(Collectors.toList()).get(0).toString());
                            }
                        }
                        System.out.println("Command: " + processBuilder.command());
                        process = processBuilder.redirectErrorStream(true).start();
                        BufferedReader reader = BAT.getReaderFrom(process);
                        Task<Object> task = new Task<>() {
                            @Override
                            protected Object call() {
                                try {
                                    String line;
                                    while ((line = reader.readLine()) != null) {
                                        String finalLine = line;
                                        Platform.runLater(() -> bat.processOutput.getChildren().addAll(new Text(finalLine + "\n")));
                                    }
                                    reader.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Platform.runLater(() -> bat.processOutput.getChildren().add(new Text("\n")));
                                System.out.println("Process finished");
                                return null;
                            }
                        };
                        Thread thread = new Thread(task);
                        thread.start();
                        bat.stopProcess.setOnAction(event1 -> {
                            process.destroy();
                            try {
                                process.waitFor();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            new Alert2(Alert.AlertType.INFORMATION, "Process destroyed, exit value " + process.exitValue()).show();
                        });
                    }
                }
            } catch (IOException | SyntaxError e) {
                e.printStackTrace();
            }
        } else {
            new Alert2(Alert.AlertType.INFORMATION, "Project must be configured and built before running", ButtonType.OK).show();
        }
    }
}
