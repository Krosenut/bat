package dev.buildtool.bat;

import dev.buildtool.json.SyntaxError;
import dev.buildtool.tools.FilePathTools;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
@Deprecated
class InstallToLocalRepository implements EventHandler<ActionEvent> {
    private final BAT bat;

    public InstallToLocalRepository(BAT bat) {
        this.bat = bat;
    }

    @Override
    public void handle(ActionEvent event) {
        LinkedHashMap<String, Object> projectProperties = null;
        try {
            projectProperties = bat.parseProjectProperties();
        } catch (SyntaxError syntaxError) {
            syntaxError.printStackTrace();
        }
        if (projectProperties != null) {
            String group = (String) projectProperties.get(Strings.ProjectProperty.PROJECT_GROUP.property);
            String artifact = (String) projectProperties.get(Strings.ProjectProperty.PROJECT_ARTIFACT.property);
            String version = (String) projectProperties.get(Strings.ProjectProperty.PROJECT_VERSION.property);
            if (group.isEmpty()) {
                new Alert2(Alert.AlertType.ERROR, "Specify group identifier", ButtonType.OK).show();
            } else if (artifact.isEmpty()) {
                new Alert2(Alert.AlertType.ERROR, "Specify artifact identifier", ButtonType.OK).show();
            } else {
                Path localBatRepository = Path.of(BAT.USER_HOME_DIR, Strings.BAT_DIR);
                if (Files.notExists(localBatRepository)) {
                    try {
                        Files.createDirectories(localBatRepository);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                Path projectFile = Path.of(Strings.BUILD_FILE);
                if (Files.exists(projectFile)) {
                    Path destination = Path.of(localBatRepository.toString(), group.replace('.', '/'), artifact, version, projectFile.toString());
                    if (Files.notExists(destination))
                        FilePathTools.copyFile(projectFile, destination);
                }

                Path jarFile = Path.of((String) projectProperties.get(Strings.ProjectProperty.JAR_DIR.property), artifact + "-" + version + ".jar");
                if (Files.exists(jarFile)) {
                    Path copyTo = Path.of(localBatRepository.toString(), group.replace('.', '/'), artifact, version, artifact + "-" + version + ".jar");
                    if (Files.notExists(copyTo)) {
                        FilePathTools.copyFile(jarFile, copyTo);
                    }
                }

                String sourceDir = (String) projectProperties.get(Strings.ProjectProperty.SOURCE_DIRECTORY.property);
                File sourceJar = new File(localBatRepository.toString(), group.replace('.', '/') + "/" + artifact + "/" + version + "/" + artifact + "-" + version + "-sources.jar");
                if (sourceDir == null) {
                    new Alert2(Alert.AlertType.ERROR, "Source directory is not defined", ButtonType.OK).show();
                } else {
                    Path sourcePath = Path.of(sourceDir);
                    List<Path> paths = FilePathTools.getFiles(sourcePath, new ArrayList<>());
                    if (!sourceJar.exists()) {
                        try {
                            JarOutputStream jarOutputStream = new JarOutputStream(new FileOutputStream(sourceJar));
                            paths.forEach(path -> {
                                String string = path.toString();
                                JarEntry jarEntry = new JarEntry(string.substring(string.indexOf(BAT.FILE_SEPARATOR) + 1));
                                try {
                                    jarOutputStream.putNextEntry(jarEntry);
                                    InputStream inputStream = Files.newInputStream(path, StandardOpenOption.READ);
                                    jarOutputStream.write(inputStream.readAllBytes());
                                    inputStream.close();
                                    jarOutputStream.closeEntry();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });
                            jarOutputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                if (Files.exists(projectFile) && Files.exists(jarFile) && sourceJar.exists()) {
                    new Alert2(Alert.AlertType.INFORMATION, "All files have been installed", ButtonType.OK).show();
                }
            }
        }
    }
}
