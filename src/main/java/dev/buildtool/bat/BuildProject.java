package dev.buildtool.bat;

import dev.buildtool.json.SyntaxError;
import dev.buildtool.tools.FilePathTools;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;

/**
 * Module conventions: name = reverse domain name, module-info location = under name, package = reverse domain name +
 * sub-package (slash-separated) under module
 */
class BuildProject implements EventHandler<ActionEvent> {
    private final BAT bat;

    public BuildProject(BAT bat) {
        this.bat = bat;
    }

    @Override
    public void handle(ActionEvent event) {
        if (bat.projectSetUp) {
            try {
                StringBuilder modulePath = new StringBuilder();
                Set<Dependency> unique = new HashSet<>(bat.dependencyLocations.keySet());
                for (Dependency entry : unique) {
                    if (entry.scope != Scope.TEST) {
                        String jarPath = bat.dependencyLocations.get(entry);
                        modulePath.append(jarPath).append(BAT.PATH_SEPARATOR);
                    }
                }
                LinkedHashMap<String, Object> projectProperties = bat.parseProjectProperties();
                if (projectProperties != null) {
                    final Path projectDir = (Path) projectProperties.get(Strings.PROJECT_LOCATION);
                    final String sourceDir = (String) projectProperties.get(Strings.ProjectProperty.SOURCE_DIRECTORY.property);
                    final String mainClass = (String) projectProperties.get(Strings.ProjectProperty.MAIN_CLASS.property);
                    final String classesDir = projectDir.toString() + "/" + projectProperties.getOrDefault(Strings.ProjectProperty.OUTPUT_DIRECTORY.property, "classes");
                    final String layout = (String) projectProperties.getOrDefault(Strings.ProjectProperty.PROJECT_LAYOUT.property, Strings.COMPACT);
                    final String bytecodeVersion = (String) projectProperties.get(Strings.ProjectProperty.TARGET_BYTECODE_VERSION.property);
                    final String resourceDir = (String) projectProperties.get(Strings.ProjectProperty.RESOURCE_DIR.property);
                    String moduleInfo;
                    List<Path> sourceFiles = FilePathTools.getFiles(Path.of(projectDir.toString(), sourceDir), new ArrayList<>());
                    List<Path> resources = FilePathTools.getFiles(Path.of(projectDir.toString(), resourceDir), new ArrayList<>());
                    resources.forEach(path -> FilePathTools.copyFile(path, Path.of(classesDir), StandardCopyOption.REPLACE_EXISTING));
                    bat.linkedProjects.forEach(path -> {
                        try {
                            Map<String, Object> properties = bat.parseProjectProperties(path);
                            String srcDir = (String) properties.get(Strings.ProjectProperty.SOURCE_DIRECTORY.property);
                            sourceFiles.addAll(FilePathTools.getFiles(Path.of(properties.get(Strings.PROJECT_LOCATION).toString(), srcDir), new ArrayList<>()));

                        } catch (SyntaxError syntaxError) {
                            syntaxError.printStackTrace();
                        }
                    });
                    boolean inModule = Files.exists(Path.of(classesDir, "module-info.class"));
                    JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
                    String[] command;
                    //compile with main class
                    if (!mainClass.isBlank()) {

                        String mainClassPath;

                        if (layout.equals(Strings.COMPACT)) {
                            mainClassPath = projectDir.resolve(sourceDir).resolve(mainClass.substring(mainClass.indexOf('/') + 1)).toString().replace('.', '/') + ".java";
                            moduleInfo = projectDir.resolve(sourceDir).resolve("module-info.java").toString();
                        } else {
                            mainClassPath = projectDir + "/" + sourceDir + "/" + mainClass;
                            moduleInfo = projectDir + "/" + sourceDir + "/" + mainClass.substring(0, mainClass.indexOf('/')) + "/" + "module-info.java";
                        }
                            if (inModule) {
                                command = new String[]{"-d", classesDir, "-source", bytecodeVersion, "-target", bytecodeVersion, "--system", System.getProperty("java.home"), "-p", modulePath.toString(), moduleInfo, mainClassPath};
                            } else
                                command = new String[]{"-d", classesDir, "-source", bytecodeVersion, "-target", bytecodeVersion, "--system", System.getProperty("java.home"), "-cp", modulePath + mainClassPath};
                    } else {
                        //compile without main class
                        command = new String[]{"-d", classesDir, "--module-path", modulePath.toString()};
                    }

                    List<String> commandList = new ArrayList<>(Arrays.asList(command));
                    //remove ?
                    for (Path sourceFile : sourceFiles) {
                        commandList.add(sourceFile.toString());
                    }
                    command = commandList.toArray(new String[0]);

                    System.out.println("Command: " + Arrays.toString(command));

                    TextFlowOutput outputStream = new TextFlowOutput(bat);

                    int result = compiler.run(null, outputStream, outputStream, command);
                    if (result == 0) {
                        new Alert2(Alert.AlertType.INFORMATION, "Compilation successful", ButtonType.CLOSE).show();
                    } else {
                        outputStream.write('\n');
                    }
                    outputStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            new Alert2(Alert.AlertType.INFORMATION, "Project must be configured before building", ButtonType.OK).show();
        }
    }

}
