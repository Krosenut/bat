package dev.buildtool.bat;

import java.util.HashMap;

/**
 * Represents XML "properties" element
 */
public class Properties {
    private final HashMap<String, String> properties = new HashMap<>();

    public String addProperty(String name, String value) {
        return properties.put(name, value);
    }

    public String getProperty(String name) {
        return properties.get(name);
    }
}
