package dev.buildtool.bat;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class Alert2 extends Alert {
    public Alert2(AlertType alertType, String headerText, ButtonType... buttons) {
        super(alertType, "", buttons);
        setHeaderText(headerText);
    }

    public Alert2(AlertType type, String header, String content, ButtonType... buttonTypes) {
        super(type, content, buttonTypes);
        setHeaderText(header);
    }
}
