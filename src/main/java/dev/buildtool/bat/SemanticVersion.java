package dev.buildtool.bat;

import java.util.Objects;

public class SemanticVersion implements Comparable<SemanticVersion> {

    private final Integer major;
    private final Integer minor;
    private final Integer patch;
    private String customVersion;

    public SemanticVersion(String versionString) {
        if (versionString.matches("\\d+")) {
            major = Integer.parseInt(versionString);
            minor = -1;
            patch = -1;
        } else if (versionString.matches("\\d+\\.\\d+")) {
            String[] vs = versionString.split("\\.");
            this.major = Integer.parseInt(vs[0]);
            this.minor = Integer.parseInt(vs[1]);
            patch = -1;
        } else if (versionString.matches("\\d+\\.\\d+\\.\\d+")) {
            String[] vs = versionString.split("\\.");
            major = Integer.parseInt(vs[0]);
            minor = Integer.parseInt(vs[1]);
            patch = Integer.parseInt(vs[2]);
        } else {
            major = 0;
            minor = 0;
            patch = 0;
            customVersion = versionString;
        }
    }

    public int getMajor() {
        return major;
    }

    public int getMinor() {
        return minor;
    }

    public int getPatch() {
        return patch;
    }

    public int compareTo(SemanticVersion other) {
        if (this.major > other.major) {
            return 1;
        }
        if (this.major < other.major) {
            return -1;
        }


        if (this.minor > other.minor) {
            return 1;
        }
        if (this.minor < other.minor) {
            return -1;
        }

        if (this.patch == null && other.patch != null) {
            return -1;
        }
        if (this.patch != null && other.patch == null) {
            return 1;
        }

        if (this.patch != null && patch > other.patch) {
            return 1;
        }
        if (this.patch != null && patch < other.patch) {
            return -1;
        }

        return 0;
    }

    @Override
    public String toString() {
        if (customVersion != null)
            return customVersion;
        if (minor == -1)
            return String.format("%d", major);
        if (this.patch == -1)
            return String.format("%d.%d", major, minor);
        return String.format("%d.%d.%d", major, minor, patch);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SemanticVersion that = (SemanticVersion) o;
        return major.equals(that.major) && minor.equals(that.minor) && patch.equals(that.patch) && Objects.equals(customVersion, that.customVersion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(major, minor, patch, customVersion);
    }
}