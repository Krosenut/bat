package dev.buildtool.bat;

import dev.buildtool.json.SyntaxError;
import dev.buildtool.tools.FilePathTools;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.text.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.jar.JarFile;
import java.util.spi.ToolProvider;
import java.util.stream.Collectors;

/**
 * Create a compressed custom runtime image with launch scripts
 */
class CreateRuntimeImage implements EventHandler<ActionEvent> {
    private final BAT bat;

    public CreateRuntimeImage(BAT bat) {
        this.bat = bat;
    }

    @Override
    public void handle(ActionEvent event) {
        if (bat.projectSetUp) {
            LinkedHashMap<String, Object> properties = null;
            try {
                properties = bat.parseProjectProperties();
            } catch (SyntaxError syntaxError) {
                syntaxError.printStackTrace();
            }
            if (properties != null) {
                String outputDir = (String) properties.get(Strings.ProjectProperty.OUTPUT_DIRECTORY.property);
                if (outputDir != null) {
                    Path outputPath = Path.of(outputDir);
                    if (Files.exists(outputPath)) {
                        if (Files.exists(Path.of(outputDir, "module-info.class"))) {
                            String mainClass = (String) properties.get(Strings.ProjectProperty.MAIN_CLASS.property);
                            if (mainClass != null) {
                                Path projectDir = (Path) properties.get(Strings.PROJECT_LOCATION);
                                String imageDir = (String) properties.getOrDefault(Strings.ProjectProperty.IMAGE_DIR.property, "images");

                                Path imagePath = Path.of(projectDir.toString(), imageDir, (String) properties.get(Strings.ProjectProperty.PROJECT_VERSION.property));
                                if (Files.exists(imagePath)) {
                                    new Alert2(Alert.AlertType.ERROR, "Directory for this version of image already exists", ButtonType.OK).show();
                                } else {
                                    StringBuilder modules = new StringBuilder();
                                    StringBuilder excludedJars = new StringBuilder();
                                    Optional<ToolProvider> jdeps = ToolProvider.findFirst("jdeps");
                                    Optional<ToolProvider> compiler = ToolProvider.findFirst("javac");
                                    Optional<ToolProvider> jarOptional = ToolProvider.findFirst("jar");
                                    PrintStream printStream = new PrintStream(new TextFlowOutput(bat));
                                    Path conversionDir = Path.of(projectDir.toString(), imageDir, "conversion");

                                    try {
                                        Files.createDirectories(conversionDir);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    for (Map.Entry<Dependency, String> entry : bat.dependencyLocations.entrySet()) {
                                        try {
                                            final String path = entry.getValue();
                                            Path jarPath = Path.of(path);
                                            if (entry.getKey().scope != Scope.TEST && Files.exists(jarPath)) {
                                                JarFile jarFile = new JarFile(path);
                                                boolean isModular = jarFile.stream().anyMatch(jarEntry -> jarEntry.getName().equals("module-info.class"));
                                                if (isModular) {
                                                    modules.append(path).append(BAT.PATH_SEPARATOR);
                                                } else {
                                                    boolean isNotEmpty = jarFile.stream().anyMatch(jarEntry -> jarEntry.getName().endsWith(".class"));
                                                    if (isNotEmpty) {
                                                        printStream.append("Trying to convert ").append(path).append('\n');
                                                        if (jdeps.isPresent()) {
                                                            ToolProvider toolJdeps = jdeps.get();
                                                            int result = toolJdeps.run(printStream, printStream, "--generate-module-info", conversionDir.toString(), path);
                                                            if (result == 0) {
                                                                var dirs = FilePathTools.getDirectories(conversionDir, new ArrayList<>());
                                                                Path subdir = dirs.stream().filter(path1 -> path1.getFileName().toString().indexOf('.') != -1).collect(Collectors.toList()).get(0);
                                                                Path tempJar = conversionDir.resolve(subdir).resolve(jarPath.getFileName());
                                                                if (Files.notExists(tempJar)) {
                                                                    Files.copy(jarPath, tempJar);
                                                                }
                                                                BAT.extract(tempJar.toString(), subdir.toString(), s -> true);
                                                                if (compiler.isPresent()) {
                                                                    ToolProvider compileTool = compiler.get();
                                                                    result = compileTool.run(printStream, printStream, "--module-path", subdir.toString(), "-d", subdir.toString(), subdir.resolve("module-info.java").toString());
                                                                    if (result == 0) {
                                                                        if (jarOptional.isPresent()) {
                                                                            ToolProvider jar = jarOptional.get();
                                                                            result = jar.run(printStream, printStream, "uf", tempJar.toString(), "-C", subdir.toString(), "module-info.class");
                                                                            if (result == 0) {
                                                                                modules.append(tempJar).append(BAT.PATH_SEPARATOR);
                                                                                printStream.append("Success").append('\n');
                                                                            } else {
                                                                                excludedJars.append(path).append('\n');
                                                                            }
                                                                        }
                                                                    } else {
                                                                        excludedJars.append(path).append('\n');
                                                                    }
                                                                }
                                                            } else {
                                                                excludedJars.append(path).append('\n');
                                                            }
                                                        }
                                                    } else {
                                                        excludedJars.append(path).append('\n');
                                                    }
                                                }
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    printStream.close();
                                    modules.append(projectDir).append("/").append(properties.get(Strings.ProjectProperty.OUTPUT_DIRECTORY.property));

                                    //main class = a.group/a.group.a.package.MainClass
                                    mainClass = mainClass.substring(0, mainClass.indexOf('/') + 1) + mainClass.substring(mainClass.indexOf('/') + 1).replace('/', '.').replace(".java", "");
                                    ProcessBuilder builder = new ProcessBuilder("jlink", "--launcher",
                                            "launch=" + mainClass, "--module-path", modules.toString(), "--add-modules",
                                            mainClass.substring(0, mainClass.indexOf('/')), "--compress=2", "--output",
                                            imagePath.toString()).redirectErrorStream(true);
                                    System.out.println(builder.command());
                                    if (excludedJars.length() > 0) {
                                        Alert2 warning = new Alert2(Alert.AlertType.WARNING, "Following jars were excluded because they aren't modular", excludedJars.toString(), ButtonType.OK);
                                        warning.setResizable(true);
                                        warning.show();
                                    }
                                    try {
                                        BufferedReader reader = BAT.getReaderFrom(builder.start());
                                        String l;
                                        while ((l = reader.readLine()) != null) {
                                            bat.processOutput.getChildren().add(new Text(l + "\n"));
                                        }
                                        bat.processOutput.getChildren().add(new Text("Finished\n"));
                                        reader.close();
                                    }
                                    catch (IOException e)
                                    {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            else
                            {
                                new Alert2(Alert.AlertType.ERROR, "Main class not specified", ButtonType.OK).show();
                            }
                        }

                    }
                }
                else
                {
                    new Alert2(Alert.AlertType.ERROR, "Output directory not specified (output_dir)", ButtonType.OK).show();
                }
            }
        } else {
            new Alert2(Alert.AlertType.INFORMATION, "Project must be configured first", ButtonType.OK).show();
        }
    }
}
