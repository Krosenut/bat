package dev.buildtool.bat;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Objects;

/**
 * Created on 5/7/20.
 */
class LibrarySearch implements EventHandler<ActionEvent>
{
    private final TextField librarySearch;
    private final ListView<String> urls = BAT.INSTANCE.urls;
    private final Label currentHyperlink = BAT.INSTANCE.currentHyperlink;
    Elements cachedHyperlinks;
    String currentLink;

    public LibrarySearch(TextField librarySearch)
    {
        this.librarySearch = librarySearch;
    }

    @Override
    public void handle(ActionEvent event)
    {
        String text = librarySearch.getText();
        Connection connection = Jsoup.connect(Strings.MAVEN_CENTRAL);
        try {
            Document document = connection.get();
            if (cachedHyperlinks == null || cachedHyperlinks.isEmpty()) {
                cachedHyperlinks = document.body().getElementsByTag("A");
            }
            urls.getItems().clear();
            for (Element element : cachedHyperlinks) {
                String hl = element.attr("href");
                if (hl.contains(text) && hl.endsWith("/")) {
                    urls.getItems().add(hl);
                }
            }
            if (urls.getItems().isEmpty()) {
                urls.getItems().add("../");
            }
            urls.setOnMouseClicked(new URLHandler());
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     * Navigates between URLs on mouse double click
     */
    private class URLHandler implements EventHandler<MouseEvent>
    {

        private final ListView<String> urls;

        private URLHandler()
        {
            this.urls = LibrarySearch.this.urls;
        }

        @Override
        public void handle(MouseEvent event)
        {
            String clicked = urls.getSelectionModel().getSelectedItem();
            if (event.getClickCount() == 2)
            {
                if (clicked.equals("../") && currentLink != null && !currentLink.equals(Strings.MAVEN_CENTRAL.substring(0, Strings.MAVEN_CENTRAL.length() - 2)))
                {
                    currentLink = currentLink.substring(0, currentLink.lastIndexOf("/", currentLink.length() - 2)) + "/";
                }
                else
                {
                    if (clicked.endsWith("/") && !clicked.equals("../"))
                    {
                        currentLink = Objects.requireNonNullElse(currentLink, Strings.MAVEN_CENTRAL) + clicked;
                    }
                }

                currentHyperlink.setText(currentLink);
                try
                {
                    Document doc = Jsoup.connect(currentLink).get();
                    Elements links = doc.getElementsByTag("a");
                    if (!links.isEmpty())
                    {
                        urls.getItems().clear();
                        cachedHyperlinks = links;
                    }
                    for (Element link : links)
                    {
                        String hlink = link.attr("href");
                        urls.getItems().add(hlink);
                    }
                }
                catch (IOException ioException)
                {
                    ioException.printStackTrace();
                    currentLink = Strings.MAVEN_CENTRAL;
                    currentHyperlink.setText(currentLink);
                    urls.getItems().clear();
                    cachedHyperlinks.clear();
                }
            }
            else if (event.getButton() == MouseButton.SECONDARY)
            {
                if (clicked.endsWith(".pom"))
                {
                    MenuItem menuItem = new MenuItem("View info");
                    menuItem.setOnAction(new PomProcessor(clicked));
                    ContextMenu contextMenu = new ContextMenu(menuItem);
                    contextMenu.show(BAT.INSTANCE.mainStage, event.getScreenX(), event.getScreenY());
                }
            }
        }

        private class PomProcessor implements EventHandler<ActionEvent>
        {
            private final String clicked;

            public PomProcessor(String clicked)
            {
                this.clicked = clicked;
            }

            @Override
            public void handle(ActionEvent event)
            {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newDefaultInstance();
                try
                {
                    StringBuilder builder = new StringBuilder();
                    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                    org.w3c.dom.Document document = documentBuilder.parse(currentLink + clicked);
                    org.w3c.dom.Element mainElement = document.getDocumentElement();
                    NodeList parent = mainElement.getElementsByTagName("parent");
                    if (parent.getLength() > 0)
                    {
                        builder.append("Parent properties:").append('\n');
                        Node node = parent.item(0);
                        NodeList childNodes = node.getChildNodes();
                        for (int i = 0; i < childNodes.getLength(); i++)
                        {
                            Node ch = childNodes.item(i);
                            if (ch instanceof org.w3c.dom.Element)
                            {
                                org.w3c.dom.Element e = (org.w3c.dom.Element) ch;
                                System.out.println(" " + e.getTagName() + " : " + ch.getTextContent());
                                builder.append(" ").append(e.getTagName()).append(" : ").append(ch.getTextContent()).append('\n');
                            }
                        }

                        NodeList artifact = mainElement.getElementsByTagName(Strings.MavenCoordinates.ARTIFACT.value);
                        builder.append("Artifact ID: ").append(artifact.item(1).getTextContent()).append('\n');
                    }
                    else
                    {
                        NodeList group = mainElement.getElementsByTagName(Strings.MavenCoordinates.GROUP.value);
                        builder.append("groupId : ").append(group.item(0).getTextContent()).append('\n');
                        NodeList artifact = mainElement.getElementsByTagName(Strings.MavenCoordinates.ARTIFACT.value);
                        builder.append("artifactId : ").append(artifact.item(0).getTextContent()).append('\n');
                        NodeList version = mainElement.getElementsByTagName(Strings.ProjectProperty.PROJECT_VERSION.property);
                        builder.append("version : ").append(version.item(0).getTextContent()).append('\n');
                    }
                    NodeList description = mainElement.getElementsByTagName("description");
                    if (description.getLength() > 0)
                    {
                        Node node = description.item(0);
                        builder.append("\nDescription:\n");
                        final String content = node.getTextContent();
                        builder.append(content).append('\n');
                    }
                    final Alert2 artifactInformation = new Alert2(Alert.AlertType.INFORMATION, "Artifact information", builder.toString(), ButtonType.CLOSE);
                    artifactInformation.setResizable(true);
                    artifactInformation.getDialogPane().setMinWidth(600);
                    artifactInformation.show();
                    System.out.println();
                }
                catch (ParserConfigurationException | SAXException | IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
