package dev.buildtool.bat;

import java.util.HashMap;

public class Profile {
    private String identifier;
    private ActivationTrigger activationTrigger;
    private final Properties properties = new Properties();
    public static final String OS = "os", OS_NAME = "name", OS_ARCHITECTURE = "arch", OS_FAMILY = "family", OS_VERSION = "version";

    public Profile(String identifier, ActivationTrigger activationTrigger) {
        this.identifier = identifier;
        this.activationTrigger = activationTrigger;
    }

    public Profile() {
    }

    @SuppressWarnings("unchecked")
    public static class ActivationTrigger {
        HashMap<String, Object> criteria = new HashMap<>(5, 1);

        void setJDK(String version) {
            criteria.put("jdk", version);
        }

        String getJDK() {
            return (String) criteria.get("jdk");
        }

        void setOS(String name, String architecture, String version, String family) {
            HashMap<String, String> hashMap = new HashMap<>(4, 1);
            hashMap.put("name", name);
            hashMap.put("arch", architecture);
            hashMap.put("family", family);
            hashMap.put("version", version);
            criteria.put("os", hashMap);
        }

        HashMap<String, String> getOS() {
            return (HashMap<String, String>) criteria.get("os");
        }

        void setProperty(String name, String value) {
            HashMap<String, String> hashMap = new HashMap<>(2, 1);
            hashMap.put("name", name);
            hashMap.put("value", value);
            criteria.put("property", hashMap);
        }

        HashMap<String, String> getProperty() {
            return (HashMap<String, String>) criteria.get("property");
        }

        void setFile(String exists, String missing) {
            HashMap<String, String> hashMap = new HashMap<>(2, 1);
            hashMap.put("exists", exists);
            hashMap.put("missing", missing);
            criteria.put("file", hashMap);
        }

        HashMap<String, String> getFile() {
            return (HashMap<String, String>) criteria.get("file");
        }

        /**
         * Tries to match the triggers
         *
         * @return true on match, false on mismatch
         */
        public boolean match() {
            HashMap<String, String> osTrigger = getOS();
            boolean osMatch = false;
            if (osTrigger != null) {
                String osName = osTrigger.get(OS_NAME);
                String osFamily = osTrigger.get(OS_FAMILY);
                String osVersion = osTrigger.get(OS_VERSION);
                String osArch = osTrigger.get(OS_ARCHITECTURE);
                boolean nameMatch = false;
                boolean familyMatch = false;
                boolean versionMatch = false;
                boolean archMatch = false;
                if (BAT.OS_NAME.equals(osName) || osName.isEmpty()) {
                    nameMatch = true;
                }
                if (BAT.OS_FAMILY.equals(osFamily) || osFamily.isEmpty()) {
                    familyMatch = true;
                }
                if (BAT.OS_ARCHITECTURE.equals(osArch) || osArch.isEmpty()) {
                    archMatch = true;
                }
                if (BAT.OS_VERSION.equals(osVersion) || osVersion.isEmpty()) {
                    versionMatch = true;
                }
                osMatch = nameMatch && familyMatch && archMatch && versionMatch;
            }
            return osMatch;
        }
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setActivationTrigger(ActivationTrigger activationTrigger) {
        this.activationTrigger = activationTrigger;
    }

    public ActivationTrigger getActivationTrigger() {
        return activationTrigger;
    }

    /**
     * Adds a property
     */
    @SuppressWarnings("UnusedReturnValue")
    public String addProperty(String name, String value) {
        return properties.addProperty(name, value);
    }

    /**
     * Gets a property value
     */
    public String getProperty(String name) {
        return properties.getProperty(name);
    }
}
