package dev.buildtool.bat;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.nio.file.Path;
import java.util.HashMap;

/**
 * For the same dependency there may many versions
 */
public class SetupProject implements EventHandler<ActionEvent> {

    protected BAT bat;
    protected HashMap<Dependency, String> dependenciesLocations = new HashMap<>();
    protected Path projectFile;

    public SetupProject(BAT bat, Path json5) {
        this.bat = bat;
        projectFile = json5;
    }

    @Override
    public void handle(ActionEvent event) {

    }
}
