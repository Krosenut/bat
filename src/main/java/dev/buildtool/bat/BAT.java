package dev.buildtool.bat;

import dev.buildtool.json.Functions;
import dev.buildtool.json.Json5Parser;
import dev.buildtool.json.Json5Serializer;
import dev.buildtool.json.SyntaxError;
import dev.buildtool.tools.FilePathTools;
import dev.buildtool.tools.Settings;
import dev.buildtool.tools.javafx.Hbox2;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;
import javafx.stage.DirectoryChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.nio.file.*;
import java.util.*;
import java.util.function.Predicate;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 5/7/20.
 */
public class BAT extends Application {
    /**
     * OS path separator - <b>:</b> or <b>;</b>
     */
    public static final String PATH_SEPARATOR = System.getProperty("path.separator");
    public static final String USER_HOME_DIR = System.getProperty("user.home");
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");

    public static final String OS_NAME = System.getProperty("os.name").toLowerCase(Locale.US);
    public static final String OS_ARCHITECTURE = System.getProperty("os.arch").toLowerCase(Locale.US);
    public static final String OS_VERSION = System.getProperty("os.version").toLowerCase(Locale.US);
    protected static final String JAVA_HOME = System.getProperty("java.home");
    public static String OS_FAMILY;

    //current version of BAT
    private static final String VERSION = "0.0.4";

    public static BAT INSTANCE;
    public static Path LOCAL_MAVEN;
    VBox mainContainer;
    ListView<String> urls;
    Label currentHyperlink, activeProject;
    Stage mainStage;
    TextFlow processOutput;
    RadioButton directory, jar;
    ToggleGroup toggleGroup;
    ChoiceBox<String> javaVersions;
    Path projectFile;
    /**
     * Locations of dependencies in local Maven
     */
    HashMap<Dependency, String> dependencyLocations;
    HashSet<Path> linkedProjects;
    boolean projectSetUp;
    Settings settings;
    File selectedDirectory;
    Button stopProcess;

    public static void main(String[] args) {
        launch(args);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void start(Stage primaryStage) {
        INSTANCE = this;
        LOCAL_MAVEN = Path.of(System.getProperty("user.home"), ".m2", "repository");
        if (OS_NAME.startsWith("windows")) {
            OS_FAMILY = "windows";
        } else if (OS_NAME.startsWith("mac")) {
            OS_FAMILY = "mac";
        } else if (OS_NAME.equals("linux")) {
            OS_FAMILY = "unix";
        }
        InputStream resource = getClass().getClassLoader().getResourceAsStream("bat_128.png");
        if (resource != null) {
            primaryStage.getIcons().add(new Image(resource));
        }
        settings = new Settings(Paths.get("settings.txt"));
        selectedDirectory = new File(settings.getOrDefault(Strings.LAST_SELECTED_DIRECTORY, USER_HOME_DIR));
        if (!selectedDirectory.isDirectory())
            selectedDirectory = new File(USER_HOME_DIR);
        dependencyLocations = new HashMap<>();
        linkedProjects = new HashSet<>();
        mainStage = primaryStage;
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        TextField librarySearch = new TextField();
        librarySearch.setTooltip(new Tooltip("Search"));
        Button search = new Button("Search for library");
        urls = new ListView<>();
        urls.setMinHeight(120);
        currentHyperlink = new Label();
        activeProject = new Label(settings.getOrDefault(Strings.LAST_PROJ_DIR, "."));
        Button checkVersion = new Button("Check for new version");
        Button configureProject = new Button("Configure project");
        Button build = new Button("Build project");
        Button buildFullJar = new Button("Build Fat jar");
        Button clean = new Button("Delete compiled classes");
        toggleGroup = new ToggleGroup();
        directory = new RadioButton("Directory");
        jar = new RadioButton("Fat Jar");
        toggleGroup.getToggles().addAll(directory, jar);
        toggleGroup.selectToggle(directory);
        Button run = new Button("Run project");
        Button createCustomImage = new Button("Build custom image");
        Button install = new Button("Install");
        Button selectProject = new Button("Select project");
        Button importGradleProject = new Button("Import Gradle project");
        processOutput = new TextFlow();
        final ScrollPane scrollPane = new ScrollPane(processOutput);
        scrollPane.setMinHeight(screen.getVisualBounds().getHeight() / 4);
        javaVersions = new ChoiceBox<>(FXCollections.observableArrayList("current", "9", "10", "11", "12", "13", "14"));
        javaVersions.getSelectionModel().select(0);
        mainContainer = new VBox(6, librarySearch, search, currentHyperlink, urls,
                new HBox(new Label("Active project: "), activeProject), new Hbox2(selectProject, importGradleProject), configureProject,
                new Hbox2(build, buildFullJar, clean), new Hbox2(new Label("Run from:"), directory, jar), new Hbox2(run, stopProcess = new Button("Stop running process")),
                scrollPane, createCustomImage, checkVersion);
        search.setOnAction(new LibrarySearch(librarySearch));
        configureProject.setOnAction(new ProjectSetup(this));
        build.setOnAction(new BuildProject(this));
        buildFullJar.setOnAction(new JarBuilder(this));
        clean.setOnAction(event -> {
            try {
                LinkedHashMap<String, Object> projectProperties = parseProjectProperties();
                Path projectdir = (Path) projectProperties.get(Strings.PROJECT_LOCATION);
                String outputdir = (String) projectProperties.get(Strings.ProjectProperty.OUTPUT_DIRECTORY.property);
                Path pathOut = Path.of(projectdir.toString(), outputdir);
                if (Files.exists(pathOut)) {
                    System.out.println("Deleting " + pathOut);
                    FilePathTools.delete(pathOut);
                    new Alert2(Alert.AlertType.INFORMATION, "Cleaned output dir").show();
                }
            } catch (SyntaxError syntaxError) {
                syntaxError.printStackTrace();
            }
        });
        selectProject.setOnAction(event -> {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            File directory = directoryChooser.showDialog(primaryStage);
            if (directory != null) {
                activeProject.setText(directory.getAbsolutePath());
                settings.put(Strings.LAST_PROJ_DIR, directory.getAbsolutePath());
            }
        });
        importGradleProject.setOnAction(new ImportGradleProject(this, primaryStage));
        checkVersion.setOnAction(event -> {
            try {
                InputStreamReader inputStreamReader = new InputStreamReader(new URL("https://alexiyorlov.github.io/bat/versions.json").openStream());
                String json = readFrom(inputStreamReader);
                final LinkedHashMap<String, List<Map<String, Object>>> map = (LinkedHashMap<String, List<Map<String, Object>>>) new Json5Parser(json).parse();
                List<Map<String, Object>> list = map.get("history");
                List<SemanticVersion> versions = new ArrayList<>(list.size());
                HashMap<SemanticVersion, List<String>> changes = new HashMap<>(list.size(), 1);
                for (Map<String, Object> stringObjectMap : list) {
                    String nextVersion = (String) stringObjectMap.get("version");
                    List<String> changeList = (List<String>) stringObjectMap.get("changes");
                    SemanticVersion version = new SemanticVersion(nextVersion);
                    versions.add(version);
                    changes.put(version, changeList);
                }
                versions.sort(null);
                SemanticVersion current = new SemanticVersion(VERSION);
                versions.remove(current);
                versions.removeIf(version -> version.compareTo(current) < 0);
                inputStreamReader.close();
                Collections.reverse(versions);
                StringBuilder stringBuilder = new StringBuilder();
                versions.forEach(version -> {
                    List<String> changeList = changes.get(version);
                    stringBuilder.append("Version ").append(version).append(": \n");
                    changeList.forEach(s -> stringBuilder.append("  ").append(s).append('\n'));
                    stringBuilder.append('\n');
                });
                Alert2 updateInfo = new Alert2(Alert.AlertType.INFORMATION,
                        versions.size() + " new versions published since current", stringBuilder.toString(),
                        ButtonType.CLOSE);
                updateInfo.getDialogPane().setMinWidth(bounds.getWidth() / 3);
                updateInfo.setResizable(true);
                updateInfo.setTitle("Changelog");
                updateInfo.show();
            } catch (IOException | SyntaxError e) {
                e.printStackTrace();
            }
        });
        Scene scene = new Scene(mainContainer, bounds.getWidth() / 2, OS_FAMILY.equals("windows") ? bounds.getHeight() - 50 : bounds.getHeight());
        primaryStage.setScene(scene);
        primaryStage.setTitle("BAT " + VERSION);

        run.setOnAction(new RunProject(this));
        createCustomImage.setOnAction(new CreateRuntimeImage(this));
        install.setOnAction(new InstallToLocalRepository(this));
        install.setTooltip(new Tooltip("Creates binary and source jars in the local BAT repository"));

        primaryStage.show();
    }

    /**
     * @return file of the active project
     */
    private Path initializeProject()
    {
        Path path = Path.of(activeProject.getText(), Strings.BUILD_FILE);
        if (Files.notExists(path))
        {
            try {
                Files.createFile(path);
                LinkedHashMap<String, String> stub = new LinkedHashMap<>(6);
                stub.put(Strings.ProjectProperty.PROJECT_GROUP.property, "");
                stub.put(Strings.ProjectProperty.PROJECT_ARTIFACT.property, "");
                stub.put(Strings.ProjectProperty.PROJECT_VERSION.property, "0.0.1");
                stub.put(Strings.ProjectProperty.SOURCE_DIRECTORY.property, "src");
                stub.put(Strings.ProjectProperty.OUTPUT_DIRECTORY.property, "classes");
                stub.put(Strings.ProjectProperty.RESOURCE_DIR.property, "resources");
                stub.put(Strings.ProjectProperty.IMAGE_DIR.property, "images");
                stub.put(Strings.ProjectProperty.JAR_DIR.property, "jars");
                stub.put(Strings.ProjectProperty.TARGET_BYTECODE_VERSION.property, "11");
                stub.put(Strings.ProjectProperty.MAIN_CLASS.property, "");

                Files.writeString(path, new Json5Serializer(stub).serialize());
            }
            catch (IOException ioException)
            {
                ioException.printStackTrace();
            }
        }
        projectFile = path;
        return path;
    }

    /**
     * Parses <i>project.json5</i> file
     *
     * @return map representation of the file + project directory
     */
    @SuppressWarnings("unchecked")
    public LinkedHashMap<String, Object> parseProjectProperties() throws SyntaxError {
        final Path path = initializeProject();
        final LinkedHashMap<String, Object> linkedHashMap = (LinkedHashMap<String, Object>) new Json5Parser(Functions.readJson(path)).parse();
        linkedHashMap.put(Strings.PROJECT_LOCATION, path.getParent());
        return linkedHashMap;
    }

    public Map<String, Object> parseProjectProperties(Path path) throws SyntaxError {
        Map<String, Object> linkedHashMap = new Json5Parser(Functions.readJson(path)).parseAsMap();
        linkedHashMap.put(Strings.PROJECT_LOCATION, path.getParent());
        return linkedHashMap;
    }

    public static BufferedReader getReaderFrom(Process process) {
        return new BufferedReader(new InputStreamReader(process.getInputStream()));
    }

    /**
     * Copies the file, creating parent directories in the process
     */
    @SuppressWarnings("UnusedReturnValue")
    public static Path copyFile(Path from, Path to, CopyOption... copyOptions) {
        try {
            if (to.getParent() != null) {
                Files.createDirectories(to.getParent());
            }
            return Files.copy(from, to, copyOptions);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return to;
    }

    /**
     * Recursively retrieves all files from the specified path
     *
     * @param from a directory
     * @param list list to populate with files
     * @return file list
     */
    public static List<Path> getFiles(Path from, List<Path> list) {
        if (Files.isDirectory(from)) {
            if (Files.isReadable(from)) {
                try {
                    Stream<Path> pathStream = Files.list(from);
                    List<Path> paths = pathStream.collect(Collectors.toList());
                    paths.forEach(path -> {
                        if (Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) {
                            getFiles(path, list);
                        } else {
                            list.add(path);
                        }
                    });
                    pathStream.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        else
        {
            throw new IllegalArgumentException(from + " is not a directory");
        }
        return list;
    }

    @Override
    public void stop() {
        settings.save();
    }

    /**
     * Recursively gets all directories
     */
    public static List<Path> getDirectories(Path from, List<Path> list) {
        if (Files.isDirectory(from)) {
            if (Files.isReadable(from)) {
                list.add(from);
                try {
                    Stream<Path> pathStream = Files.list(from);
                    List<Path> paths = pathStream.collect(Collectors.toList());
                    paths.forEach(path -> {
                        if (Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) {
                            getDirectories(path, list);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            throw new IllegalArgumentException(from + " is not a directory");
        }
        return list;
    }

    /**
     * Recursively deletes files and folders
     *
     * @param path file or dir
     * @return true on success
     */
    public static boolean delete(Path path) {
        if (Files.isDirectory(path)) {
            List<Path> files = FilePathTools.getFiles(path, new ArrayList<>());
            for (Path file : files) {
                delete(file);
            }

            List<Path> folders = FilePathTools.getDirectories(path, new ArrayList<>());
            Collections.reverse(folders);
            for (Path directory : folders) {
                try {
                    if (Files.isWritable(directory))
                        Files.deleteIfExists(directory);
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }

            try {
                return Files.deleteIfExists(path);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            try {
                if (Files.isWritable(path))
                    return Files.deleteIfExists(path);
                else
                    return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    /**
     * Extracts contents of a Jar to a directory
     *
     * @param jarFile
     * @param outputDir
     * @return true on success, false on failure
     */
    static boolean extract(String jarFile, String outputDir, Predicate<String> fileFilter) {
        try {
            JarInputStream jarInputStream = new JarInputStream(new FileInputStream(jarFile));
            JarEntry jarEntry;
            Path output = Path.of(outputDir);
            if (Files.notExists(output))
                Files.createDirectory(output);
            while ((jarEntry = jarInputStream.getNextJarEntry()) != null) {
                if (jarEntry.isDirectory()) {
                    Files.createDirectories(output.resolve(jarEntry.getName()));
                } else {
                    if (fileFilter.test(jarEntry.getName())) {
                        byte[] bytes = jarInputStream.readAllBytes();
                        Path resolved = output.resolve(jarEntry.getName());
                        if (Files.notExists(resolved.getParent()))
                            Files.createDirectories(resolved.getParent());
                        Files.write(resolved, bytes);
                    }
                }
                jarInputStream.closeEntry();
            }
            jarInputStream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Creates a Jar from the given directory files
     *
     * @param inputDir  from
     * @param outputJar to
     */
    static void archive(Path inputDir, File outputJar) {
        List<Path> paths = FilePathTools.getFiles(inputDir, new ArrayList<>());
        if (!outputJar.exists()) {
            try {
                JarOutputStream jarOutputStream = new JarOutputStream(new FileOutputStream(outputJar));
                paths.forEach(path -> {
                    String string = inputDir.relativize(path).toString();
                    JarEntry jarEntry = new JarEntry(string);
                    try {
                        jarOutputStream.putNextEntry(jarEntry);
                        InputStream inputStream = Files.newInputStream(path, StandardOpenOption.READ);
                        jarOutputStream.write(inputStream.readAllBytes());
                        inputStream.close();
                        jarOutputStream.closeEntry();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                jarOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static String readFrom(InputStreamReader inputStream) {
        return new BufferedReader(inputStream).lines().collect(Collectors.joining("\n"));
    }

}
