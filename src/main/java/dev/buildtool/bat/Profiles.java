package dev.buildtool.bat;

import java.util.ArrayList;
import java.util.Arrays;

public class Profiles {
    private final ArrayList<Profile> profiles = new ArrayList<>();
    ;

    public Profiles() {
    }

    public Profiles(Profile... profiles) {
        this.profiles.addAll(Arrays.asList(profiles));
    }

    ArrayList<Profile> getProfiles() {
        return profiles;
    }

    void addProfile(Profile profile) {
        profiles.add(profile);
    }
}
