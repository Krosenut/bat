package dev.buildtool.bat;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Optional;
import java.util.spi.ToolProvider;

class JarBuilder implements EventHandler<ActionEvent> {
    private final BAT bat;

    public JarBuilder(BAT bat) {
        this.bat = bat;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        if (bat.projectSetUp) {
            try {
                Optional<ToolProvider> jarTool = java.util.spi.ToolProvider.findFirst("jar");
                if (jarTool.isPresent()) {
                    LinkedHashMap<String, Object> projectProperties = bat.parseProjectProperties();
                    final Path projectDir = (Path) projectProperties.get(Strings.PROJECT_LOCATION);

                    String jarOutput = (String) projectProperties.getOrDefault(Strings.ProjectProperty.JAR_DIR.property, "jars");
                    Path jarPath = Path.of(projectDir.toString(), jarOutput);
                    if (Files.notExists(jarPath)) {
                        Files.createDirectory(jarPath);
                    }
                    final String mainClass = (String) projectProperties.get(Strings.ProjectProperty.MAIN_CLASS.property);
                    final String layout = (String) projectProperties.getOrDefault(Strings.ProjectProperty.PROJECT_LAYOUT.property, Strings.COMPACT);
                    final String classesDir = projectDir + "/" + projectProperties.getOrDefault(Strings.ProjectProperty.OUTPUT_DIRECTORY.property, "classes");
                    boolean inModule = Files.exists(Path.of(classesDir, "module-info.class"));

                    java.util.spi.ToolProvider jarProvider = jarTool.get();
                    TextFlowOutput flowOutput = new TextFlowOutput(bat);
                    PrintStream stream = new PrintStream(flowOutput);
                    String group = (String) projectProperties.get(Strings.ProjectProperty.PROJECT_GROUP.property);
                    String shortClassName;
                    if (inModule) {
                        if (layout.equals("compact")) {
                            shortClassName = mainClass.substring(mainClass.indexOf('/') + 1);
                        } else {
                            shortClassName = mainClass.substring(mainClass.indexOf('/') + 1).replace('/', '.').replace(group, "").replace(".java", "").substring(1);
                        }
                    } else shortClassName = mainClass.replace('/', '.').replace(".java", "");

                    bat.dependencyLocations.forEach((dependency, s) -> {
                        if (Files.exists(Path.of(s)) && dependency.scope != Scope.TEST) {
                            if (!BAT.extract(s, classesDir, s1 -> s1.endsWith(".class") && !s1.equals("module-info.class") && !s1.contains("META-INF")))
                                System.out.println("Failed to extract " + s);
                        }
                    });

                    final String jarFileToBe = jarPath + "/" + projectProperties.get(Strings.ProjectProperty.PROJECT_ARTIFACT.property)
                            + "-" + projectProperties.get(Strings.ProjectProperty.PROJECT_VERSION.property) + ".jar";
                    String[] arguments = new String[]{"--create", "--file", jarFileToBe, "--main-class", shortClassName, "-C", classesDir, "."};
                    System.out.println("Command: " + Arrays.toString(arguments));
                    int exit = jarProvider.run(stream, stream, arguments);
                    if (exit != 0) {
                        flowOutput.write('\n');
                    } else {
                        new Alert2(Alert.AlertType.INFORMATION, "Jar creation succeeded", ButtonType.FINISH).show();
                    }

                    flowOutput.close();
                    stream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
