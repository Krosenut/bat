package dev.buildtool.bat;

import dev.buildtool.json.Json5Serializer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

class ImportGradleProject implements EventHandler<ActionEvent> {
    private final BAT bat;
    private final Stage primaryStage;

    public ImportGradleProject(BAT bat, Stage primaryStage) {
        this.bat = bat;
        this.primaryStage = primaryStage;
    }

    @Override
    public void handle(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select a 'build.gradle' file");
        fileChooser.setInitialDirectory(bat.selectedDirectory);
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Gradle script", "*.gradle"));
        File selected = fileChooser.showOpenDialog(primaryStage);
        if (selected != null) {
            bat.selectedDirectory = selected.getParentFile();
            try {
                var strings = Files.readAllLines(selected.toPath());
                var trimmed = strings.stream().map(String::strip).collect(Collectors.toList());
                LinkedHashMap<String, Object> projectProperties = new LinkedHashMap<>(trimmed.size());
                List<Map<String, String>> dependencies = new ArrayList<>();
                trimmed.forEach(s -> {
                    if (s.startsWith("group")) {
                        String group = s.replaceFirst("group", "").strip();
                        group = group.substring(1, group.length() - 1);
                        projectProperties.put(Strings.ProjectProperty.PROJECT_GROUP.property, group);
                    } else if (s.startsWith("version")) {
                        String version = s.replaceFirst("version", "").strip();
                        version = version.substring(1, version.length() - 1);
                        projectProperties.put(Strings.ProjectProperty.PROJECT_VERSION.property, version);
                    } else if (s.startsWith("attributes")) {
                        String startClass = s.substring(s.indexOf("Main-Class"));
                        System.out.println(startClass);
                        String[] mainClass = startClass.split(":");
                        projectProperties.put(Strings.ProjectProperty.MAIN_CLASS.property, mainClass[1].substring(1, mainClass[1].length() - 1));
                    } else if (s.startsWith("implementation")) {
                        String compileDep = s.replaceFirst("implementation", "").strip();
                        if (compileDep.startsWith("(")) {
                            compileDep = compileDep.substring(1, compileDep.length() - 1);
                        }
                        if (compileDep.contains("group:") && compileDep.contains("name:")) {
                            String group = compileDep.substring(compileDep.indexOf(": ") + 3, compileDep.indexOf(',') - 1);
                            String artifact = compileDep.substring(compileDep.indexOf(", name: ") + 9, compileDep.lastIndexOf(',') - 1);
                            String version = compileDep.substring(compileDep.lastIndexOf(": ") + 3, compileDep.length() - 1);
                            LinkedHashMap<String, String> dependency = new LinkedHashMap<>(3);
                            dependency.put(Strings.ProjectProperty.PROJECT_GROUP.property, group);
                            dependency.put(Strings.ProjectProperty.PROJECT_ARTIFACT.property, artifact);
                            dependency.put(Strings.ProjectProperty.PROJECT_VERSION.property, version);
                            dependencies.add(dependency);
                        } else if (compileDep.equals(":")) {
                            //TODO root
                        } else {
                            String[] parts = compileDep.substring(1, compileDep.length() - 1).split(":");
                            LinkedHashMap<String, String> dependency = new LinkedHashMap<>(3);
                            dependency.put(Strings.ProjectProperty.PROJECT_GROUP.property, parts[0]);
                            dependency.put(Strings.ProjectProperty.PROJECT_ARTIFACT.property, parts[1]);
                            dependency.put(Strings.ProjectProperty.PROJECT_VERSION.property, parts[2]);
                            dependencies.add(dependency);
                        }

                    }

                });
                projectProperties.put(Strings.ProjectProperty.DEPENDENCIES.property, dependencies);
                File parent = selected.getParentFile();
                File[] files = parent.listFiles();
                assert files != null;
                for (File file : files) {
                    if (file.isDirectory()) {
                        if (file.getName().equals("build")) {
                            projectProperties.put(Strings.ProjectProperty.OUTPUT_DIRECTORY.property, "build/classes/java/main");
                        }
                    }

                }
                projectProperties.put(Strings.ProjectProperty.SOURCE_DIRECTORY.property, "src/main/java");
                Path projectJson = Path.of(parent.toString(), "project.json5");
                if (Files.notExists(projectJson)) {
                    Files.createFile(projectJson);
                }
                Files.write(projectJson, Collections.singleton(new Json5Serializer(projectProperties).serialize()), StandardOpenOption.TRUNCATE_EXISTING);
                bat.activeProject.setText(parent.getAbsolutePath());
                bat.settings.put(Strings.LAST_PROJ_DIR, parent.getAbsolutePath());
                bat.settings.put(Strings.LAST_SELECTED_DIRECTORY, parent.getAbsolutePath());
                new Alert2(Alert.AlertType.INFORMATION, "Success!", "Project " + parent.getName() + " has been imported", ButtonType.FINISH).show();
            } catch (IOException e) {
                e.printStackTrace();
                new Alert2(Alert.AlertType.ERROR, "Failed to parse build script", ButtonType.FINISH).show();
            } catch (Exception e) {
                e.printStackTrace();
                new Alert2(Alert.AlertType.ERROR, e.getClass().getSimpleName(), e.getLocalizedMessage()).show();
            }
        }
    }
}
