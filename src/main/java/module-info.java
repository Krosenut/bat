/**
 * Created on 5/7/20.
 */
module dev.buildtool {
    exports dev.buildtool.bat;
    requires javafx.controls;
    requires javafx.graphics;
    requires org.jsoup;
    requires java.xml;
    requires java.compiler;
    requires dev.buildtool.json;
    requires dev.buildtool.tools;
}