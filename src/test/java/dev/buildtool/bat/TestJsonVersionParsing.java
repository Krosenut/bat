package dev.buildtool.bat;


import dev.buildtool.json.Json5Parser;
import dev.buildtool.json.SyntaxError;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 5/16/20.
 */

public class TestJsonVersionParsing {
    @Test
    @SuppressWarnings("unchecked")
    public void testVersionCheck() {
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(new URL("file:versions.json").openStream());
            String json = BAT.readFrom(inputStreamReader);
            final LinkedHashMap<String, List<Map<String, Object>>> map = (LinkedHashMap<String, List<Map<String, Object>>>) new Json5Parser(json).parse();
            List<Map<String, Object>> list = map.get("history");
            System.out.println(list.size() + " versions");
            List<SemanticVersion> versions = new ArrayList<>(list.size());
            for (Map<String, Object> stringObjectMap : list) {
                String nextVersion = (String) stringObjectMap.get("version");
                List<String> changeList = (List<String>) stringObjectMap.get("changes");
                System.out.println(nextVersion + " " + changeList);
                versions.add(new SemanticVersion(nextVersion));
            }
            versions.sort(SemanticVersion::compareTo);
            System.out.println("Latest " + versions.get(versions.size() - 1));
            inputStreamReader.close();
        } catch (IOException | SyntaxError ioException) {
            ioException.printStackTrace();
        }
    }
}
