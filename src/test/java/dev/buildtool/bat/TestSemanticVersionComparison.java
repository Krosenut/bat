package dev.buildtool.bat;

import org.junit.jupiter.api.Test;

public class TestSemanticVersionComparison {

    @Test
    public void testVersionComparison() {
        SemanticVersion version = new SemanticVersion("1.0.0");
        SemanticVersion higher = new SemanticVersion("1.2.0");
        SemanticVersion lower = new SemanticVersion("0.0.5");

        assert higher.compareTo(version) > 0;
        assert lower.compareTo(version) < 0;
        assert new SemanticVersion("1.0.0").compareTo(version) == 0;

        SemanticVersion lower2 = new SemanticVersion("0.22.0");
        SemanticVersion higher2 = new SemanticVersion("1.0.15");

        assert lower2.compareTo(version) < 0;
        assert higher2.compareTo(version) > 0;

    }

    @Test
    public void testTwoVersions() {
        SemanticVersion version1 = new SemanticVersion("1.16.4");
        SemanticVersion version2 = new SemanticVersion("1.8");
        assert version1.compareTo(version2) > 0;
    }
}
