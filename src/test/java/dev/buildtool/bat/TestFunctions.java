package dev.buildtool.bat;

import org.junit.jupiter.api.Test;

public class TestFunctions {

    @Test
    public void testBackwardSearch() {
        String twodots = "one.two.three";
        assert ProjectSetup.findCharBackwards(twodots, '.', 2) == 3;
        assert ProjectSetup.findCharBackwards(twodots, '.', 1) == 7;
    }
}
